package blackwater.utils.string;

import blackwater.Template;

import java.util.*;

/**
 * Created by asolod on 9/13/14.
 */
public class SentenceSplitter {


    public static String[] parseByWords(String sentence) {
        return sentence.split(" ");
    }

    public static String calculateSalt(String sentence) {
        String result = "";
        for (String word : parseByWords(sentence)) {

            if (word.contains("{") && word.contains("}")) {
                result += word;
            } else {
                result += word.substring(0, 1);
            }
        }
        return result;
    }

    public static List<Template> generateTamplates(String originalString, Integer substCount) {
        List<Template> templates = new LinkedList<Template>();
        for (String word : parseByWords(originalString)) {
            templates.add(new Template(originalString.replaceFirst(word, "{0}")));
        }
        return templates;
    }

    public static Set<Template> generateMultipeTamplates(Set<Template> templates, Integer idx) {
        Set<Template> newTemplates = new HashSet<Template>();
        //newTemplates.addAll(templates);
        for (Template template : templates) {
            String[] words = template.getFormat().split(" ");
            for (int i=idx; i<words.length; i++) {
                if (words[i].indexOf("{") == -1) {
                    newTemplates.add(new Template(template.getFormat().replaceAll(words[i], "{" + idx + "}")));
                }
            }
        }
        return newTemplates;
    }

    public static Set<Template> generateMultipeTamplates(String originalString) {
        Set<Template> newTemplates = new HashSet<Template>();
        Set<Template> templates = new HashSet<Template>();

        newTemplates.add(new Template(originalString));
        templates.add(new Template(originalString));

        String[] words = parseByWords(originalString);
        for (int i = 0; i < words.length; i++) {
            newTemplates = generateMultipeTamplates(newTemplates, i);
            System.out.println(newTemplates);
            templates.addAll(newTemplates);
        }
        return templates;
    }

    public static List<Template> generateTemplates(final String sens) {
        StringTokenizer stringTokenizer = new StringTokenizer(sens);       // remove , . / -
        int i = 0;
        List<Template> templates = new LinkedList<Template>();
        while (stringTokenizer.hasMoreElements()) {
            String word = stringTokenizer.nextToken();
            String pattern = sens.replace(word, "{0}");
            templates.add(new Template(pattern));
        }
        return templates;
    }

    public static void main(String[] args) {
        System.out.println(generateMultipeTamplates("Hello Artem to hell").toString().replaceAll(",", "\n"));
     /*
        System.out.println(calculateSalt("Hello Artem"));
        System.out.println(generateTemplates("Size: 194 KB (20 KB + 174 KB)"));
        */
/*
        System.out.println(new File("applicationContext.xml").getAbsolutePath());
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
*/

    }

}

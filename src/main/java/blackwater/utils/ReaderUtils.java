package blackwater.utils;

import blackwater.datamaning.Bucket;
import blackwater.datamaning.Bucketer;
import blackwater.datamaning.ReportWriter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Gorets on 13.09.14.
 */
public class ReaderUtils {

    Map<String, Integer> stringIntegerMap;

    public ReaderUtils() {
        this.stringIntegerMap = new HashMap<String, Integer>();
    }

    public void readFile(String fileName) throws IOException {
        Bucketer bucketer = new Bucketer();
        BufferedReader in = new BufferedReader(new FileReader(fileName));

        while (in.ready()) {
            String line = in.readLine();
            String[] splitByLineDelim = line.split("\\|");
            for (String s : splitByLineDelim) {
                //System.out.println("Read string from file " + s);
                bucketer.bucketNewString(s.trim());
            }
        }
        in.close();

        System.out.println(new Date() + " started grouping buckets");

        Map<String, Bucket> groupedBackets = new HashMap<String, Bucket>();
        for (Bucket bucket : bucketer.getBuckets()) {
            if (bucket.getSentenceLength() != -1 && bucket.getStrings().size() > 1) {
                Bucket existingBucket = groupedBackets.get(bucket.generatePattern());
                if (groupedBackets.get(bucket.generatePattern()) == null) {
                    groupedBackets.put(bucket.generatePattern(), bucket);
                } else {
                    existingBucket.getStrings().addAll(bucket.getStrings());
                }

            }
        }

        System.out.println(new Date() + " started generating report");
        ReportWriter writer = new ReportWriter();
        writer.start();

        for (Bucket bucket : groupedBackets.values()) {
            if (bucket.getStrings().size() > 1) {
                writer.addPanelHeder("(" + bucket.getStrings().size() + ") </br>"
                                + "<xmp>" + bucket.generatePattern() + "</xmp></br>"
                        /*+ bucket.getFirstSentence().getIndexCard().getFromStartIdxs()+" </br>"
                        + bucket.getLastSentence().getIndexCard().getFromStartIdxs()+""*/);

                writer.addPanelBody(collectionToString(bucket.getStrings()));
            }
        }


        for (Bucket bucket : groupedBackets.values()) {
            if (bucket.getStrings().size() ==1) {
                writer.addPanelHeder("(" + bucket.getStrings().size() + ") no pattern available");
                writer.addPanelBody(bucket.getStrings().toString().replaceAll(",", "</br>"));
            }
        }

        writer.close();
        System.out.println(new Date() + " finshed generating report");

    }

    private String collectionToString(Collection collection) {
        String s = "";
        for (Object item : collection) {
            s += item.toString() + "</br>";
        }
        return s;
    }

    public static void main(String[] args) throws IOException {
        //new ReaderUtils().readFile("/home/asolod/learning/blackWater/src/main/java/resources/test_small.html");
        new ReaderUtils().readFile("/home/asolod/learning/blackWater/src/main/java/resources/test_bigger_2.html");
        //new ReaderUtils().readFile("/home/asolod/learning/blackWater/src/main/java/resources/test_data_for_patterns.txt");

        //    System.out.println(IndexCardsMatcher.match(new Sentence(" <span>Size: 194 KB (20 KB + 174 KB)</span> "), new Sentence(" <span>Derived from: <a href=\"{1}\">Derivable Oval R...</a></span> ")));
    }
}

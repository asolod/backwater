package blackwater.datamaning;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by asolod on 9/13/14.
 */

public class IndexCard {

    private Map<Integer, String> fromStartIdxs = new HashMap<Integer, String>();
    private Map<Integer, String> fromEndIdxs = new HashMap<Integer, String>();

    public Map<Integer, String> getFromStartIdxs() {
        return fromStartIdxs;
    }

    public void setFromStartIdxs(Map<Integer, String> fromStartIdxs) {
        this.fromStartIdxs = fromStartIdxs;
    }

    public Map<Integer, String> getFromEndIdxs() {
        return fromEndIdxs;
    }

    public void setFromEndIdxs(Map<Integer, String> fromEndIdxs) {
        this.fromEndIdxs = fromEndIdxs;
    }

    @Override
    public String toString() {
        return "IndexCard{" +
                "fromStartIdxs=" + fromStartIdxs +
                ", fromEndIdxs=" + fromEndIdxs +
                '}';
    }
}

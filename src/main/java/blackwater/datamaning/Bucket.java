package blackwater.datamaning;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by asolod on 9/13/14.
 */
public class Bucket {

    private List<String> strings = new LinkedList<String>();
    private Sentence firstSentence;
    private Sentence lastSentence;


    private int sentenceLength;

    public Bucket(String newString) {
        this.strings.add(newString);
        this.sentenceLength = new Sentence(newString).getIndexCard().getFromEndIdxs().size();
        this.firstSentence = new Sentence(newString);
    }

    public Float matchString(Sentence newSentence) {
        return IndexCardsMatcher.match(firstSentence, newSentence);
    }

    public void addString(Sentence newSentence) {
        this.strings.add(newSentence.getOriginalString());
        this.lastSentence = newSentence;
        if (sentenceLength != newSentence.getIndexCard().getFromEndIdxs().size()) {
            sentenceLength = -1;
        }
    }

    public Sentence getLastSentence() {
        return lastSentence;
    }

    public void setLastSentence(Sentence lastSentence) {
        this.lastSentence = lastSentence;
    }

    public Sentence getFirstSentence() {
        return firstSentence;
    }

    public void setFirstSentence(Sentence firstSentence) {
        this.firstSentence = firstSentence;
    }

    public List<String> getStrings() {
        return strings;
    }

    public void setStrings(List<String> strings) {
        this.strings = strings;
    }

    public int getSentenceLength() {
        return sentenceLength;
    }

    public void setSentenceLength(int sentenceLength) {
        this.sentenceLength = sentenceLength;
    }

    public String generatePattern() {
        if (sentenceLength != -1 && strings.size() > 1)
            return generateSimplePattern();
        if (strings.size() > 1)
            return generateCustomPattern();
        return null;
    }

    private String generateSimplePattern() {
        String pattern = "";
        IndexCard card1 = firstSentence.getIndexCard();
        IndexCard card2 = lastSentence.getIndexCard();
        int wildCardIdx = 0;

        for (Integer i = 0; i < card1.getFromStartIdxs().size(); i++) {
            if (card1.getFromStartIdxs().get(i).equals(card2.getFromStartIdxs().get(i))) {
                pattern += " " + card1.getFromStartIdxs().get(i);
            } else {
                if (pattern.lastIndexOf('#') != pattern.length() - 1) {
                    pattern += " #" + wildCardIdx + "#";
                    wildCardIdx++;
                }
            }

        }

        return SentenceParser.removeWhitespaces(pattern).trim();
    }

    private String generateCustomPattern() {
        String pattern = "";
        IndexCard card1 = firstSentence.getIndexCard();
        IndexCard card2 = lastSentence.getIndexCard();
        int wildCardIdx = 0;
        IndexTuple tuple = new IndexTuple(0, 0);

        while (tuple.getIdx1() < card1.getFromStartIdxs().size()) {

            if (card1.getFromStartIdxs().get(tuple.getIdx1()).equals(card2.getFromStartIdxs().get(tuple.getIdx2()))) {
                pattern += card1.getFromStartIdxs().get(tuple.getIdx1()) + " ";
                tuple.incrementIdx1();
                tuple.incrementIdx2();
            } else {
                if (pattern.lastIndexOf('#') != pattern.length() - 1) {
                    pattern += " #" + wildCardIdx + "#";
                    wildCardIdx++;
                }
//                pattern += "#" + wildCardIdx + "#";
                scrollIndexes(tuple, card1.getFromStartIdxs(), card2.getFromStartIdxs());
            }
        }
        return SentenceParser.removeWhitespaces(pattern).trim();
    }

    private void scrollIndexes(IndexTuple tuple, Map<Integer, String> fromStartIdxs1, Map<Integer, String> fromStartIdxs2) {
        while (1 == 1) {

            for (int i = tuple.getIdx2(); i < fromStartIdxs2.size(); i++) {
                if (fromStartIdxs1.get(tuple.getIdx1()).equals(fromStartIdxs2.get(i))) {
                    tuple.setIdx2(i);
                    return;
                }
            }

            tuple.incrementIdx1();
        }
    }

    @Override
    public String toString() {
        return "Bucket{" +
                "size=" + strings.size() +
                ", pattern=" + generatePattern() + " " +
                "strings=" + strings +
                ", sentenceLength=" + sentenceLength +
                "}\n";
    }
}

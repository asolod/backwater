package blackwater.datamaning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * Created by asolod on 9/14/14.
 */
public class ReportWriter {

    private PrintWriter writer;
    private int i=0;

    public void start() throws FileNotFoundException, UnsupportedEncodingException {
        writer = new PrintWriter("the-file-name.html", "UTF-8");
        write("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>");
        write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\">\n");
        write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css\">\n");
        write("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js\"></script>\n");
        write("<div class=\"panel-group\" id=\"accordion\" style=\"padding: 30px\">\n");
    }

    public void addPanelHeder(String title) {
        i++;
        write("<div class=\"panel panel-default\">\n");
        write("<div class=\"panel-heading\">");
        write("<h4 class=\"panel-title\">");
        write("<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + i + "\">");
        write(title);
        write("</a></h4>");
        write("</div>");
    }

    public void addPanelBody(String title) {
        write("<div id=\"collapse"+i+"\" class=\"panel-collapse collapse in\">\n");
        write("<div class=\"panel-body\">\n");
        write(title);
        write("</div>\n</div>\n</div>");
        writer.flush();
    }

    public void write(String s) {
        writer.println(s);
    }

    public void close() {
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        ReportWriter reportWriter = new ReportWriter();
        reportWriter.start();
        reportWriter.addPanelHeder("title");
        reportWriter.addPanelBody("body");
        reportWriter.close();
    }
}

package blackwater.datamaning;

/**
 * Created by asolod on 9/14/14.
 */
public class IndexTuple {

    private Integer idx1;
    private Integer idx2;

    public IndexTuple(Integer idx1, Integer idx2) {
        this.idx1 = idx1;
        this.idx2 = idx2;
    }

    public void incrementIdx1() {
        idx1++;
    }

    public void incrementIdx2() {
        idx2++;
    }

    public void setIdx2(Integer idx2) {
        this.idx2 = idx2;
    }

    public Integer getIdx1() {
        return idx1;
    }

    public Integer getIdx2() {
        return idx2;
    }

    @Override
    public String toString() {
        return "IndexTuple{" +
                "idx1=" + idx1 +
                ", idx2=" + idx2 +
                '}';
    }
}

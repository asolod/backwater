package blackwater.datamaning;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by asolod on 9/13/14.
 */
public class Sentence {

    private String originalString;
    private IndexCard indexCard;

    public Sentence(String originalString) {
        this.originalString = originalString;
        this.indexCard = SentenceParser.generateIndexCard(originalString);
    }

    public String getOriginalString() {
        return originalString;
    }

    public void setOriginalString(String originalString) {
        this.originalString = originalString;
    }

    public IndexCard getIndexCard() {
        return indexCard;
    }

    public void setIndexCard(IndexCard indexCard) {
        this.indexCard = indexCard;
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "originalString='" + originalString + '\'' +
                ", indexCard=" + indexCard +
                '}';
    }

    public static void main(String[] args) {
        List<String> words = new LinkedList<String>() {{
            add("Size: 343 KB (118 KB + 225 KB)");
            add("Size: 123 KB (121 KB + 321 KB)");
            add("<span>Derived from: <a href=\"{1}\">w1 w2 </a></span>");
            add("<span>Derived from: <a href=\"{1}\">w1 w2 w3 w4 </a></span>");
        }};

/*
        for (String word:words){
            System.out.println(new Sentence(word));
        }
        System.out.println(IndexCardsMatcher.match(new Sentence(words.get(0)).getIndexCard(), new Sentence(words.get(1)).getIndexCard()));
        System.out.println(IndexCardsMatcher.match(new Sentence(words.get(2)).getIndexCard(), new Sentence(words.get(3)).getIndexCard()));
        System.out.println(IndexCardsMatcher.match(new Sentence(words.get(0)).getIndexCard(), new Sentence(words.get(2)).getIndexCard()));
*/
        Bucketer bucketer = new Bucketer();
        bucketer.bucketNewString(words.get(0));
        bucketer.bucketNewString(words.get(1));
        bucketer.bucketNewString(words.get(2));
        bucketer.bucketNewString(words.get(3));
        System.out.println(bucketer);
    }

}

package blackwater.datamaning;

/**
 * Created by asolod on 9/13/14.
 */
public class SentenceParser {

    public static String addWhitespaces(String s) {
        return s.replaceAll("\\(", "( ").replaceAll("\\)", " )").replaceAll(">","> ").replaceAll("<", " <");
    }

    public static String removeWhitespaces(String s) {
        return s.replaceAll("\\( ", "(").replaceAll(" \\)", ")").replaceAll("> ",">").replaceAll(" <", "<");
    }

    public static IndexCard generateIndexCard(String originalString) {
        IndexCard card = new IndexCard();
        String[] words = addWhitespaces(originalString).split(" ");
        for (int i = 0; i < words.length; i++) {
            card.getFromStartIdxs().put(i, words[i]);
            card.getFromEndIdxs().put(words.length - i, words[i]);
        }
        return card;
    }
}

package blackwater.datamaning;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by asolod on 9/13/14.
 */
public class Bucketer {

    List<Bucket> buckets = new LinkedList<Bucket>();

    public List<Bucket> getBuckets() {
        return buckets;
    }

    public void bucketNewString(String newString) {

       // checking new
       Sentence newSentence = new Sentence(newString);
        for (Bucket bucket : buckets) {
            if (bucket.matchString(newSentence) > 1f) {
                bucket.addString(newSentence);
                return;
            }
        }

        // if not found similar
        buckets.add(new Bucket(newString));
    }

    @Override
    public String toString() {
        return "Bucketer{" +
                "buckets=" + buckets +
                '}';
    }
}

package blackwater.datamaning;

import java.util.Map;

/**
 * Created by asolod on 9/13/14.
 */
public class IndexCardsMatcher {

    public static Float match(Sentence sentence1, Sentence sentence2) {
        return match(sentence1.getIndexCard(), sentence2.getIndexCard());
    }

    public static Float match(IndexCard card1, IndexCard card2) {
        Float wordsQty = 1.0f * Math.min(card1.getFromStartIdxs().size(), card2.getFromStartIdxs().size());
        Float matcheaQty = 1.0f * compareIndexes(card1.getFromStartIdxs(), card2.getFromStartIdxs()) + compareIndexes(card1.getFromEndIdxs(), card2.getFromEndIdxs());
        return matcheaQty / wordsQty;
    }

    private static Integer compareIndexes(Map<Integer, String> fromStartIdxs1, Map<Integer, String> fromStartIdxs2) {
        int matches = 0;
        for (Integer idx : fromStartIdxs1.keySet()) {
            if (fromStartIdxs1.get(idx).equals(fromStartIdxs2.get(idx))) {
                matches++;
            }
        }
        return matches;
    }
}

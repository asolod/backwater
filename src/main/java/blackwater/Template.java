package blackwater;

import blackwater.utils.string.SentenceSplitter;

/**
 * Created by asolod on 9/13/14.
 */
public class Template {

    private String format;
    private String salt;

    public Template(String format) {
        this.format = format;
        this.salt = SentenceSplitter.calculateSalt(format);
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public String toString() {
        return "Template{" +
                "format='" + format + '\'' +
                ", salt='" + salt + '\'' +
                '}';
    }
}

package blackwater;

import blackwater.utils.string.SentenceSplitter;

import java.util.List;

/**
 * Created by asolod on 9/13/14.
 */
public class Sentence {

    private String originalSentence;
    private List<Template> templates;

    public Sentence(String originalSentence) {
        this.originalSentence = originalSentence;
        this.templates = SentenceSplitter.generateTamplates(originalSentence, 0);
    }

    public String getOriginalSentence() {
        return originalSentence;
    }

    public void setOriginalSentence(String originalSentence) {
        this.originalSentence = originalSentence;
    }

    public List<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(List<Template> templates) {
        this.templates = templates;
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "originalSentence='" + originalSentence + '\'' +
                ", templates=" + templates +
                '}';
    }

    public static void main(String[] args) {
        System.out.println(new Sentence("Hello, Artem !! "));
    }

}
